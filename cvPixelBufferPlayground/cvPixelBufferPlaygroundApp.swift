//
//  cvPixelBufferPlaygroundApp.swift
//  cvPixelBufferPlayground
//
//  Created by Sanja Mihajlovic on 11/8/21.
//

import AVFoundation
import CoreVideo
import SwiftUI

class AVSampleBufferNSView: NSView {
    var videoLayer: AVSampleBufferDisplayLayer!

    override func layout() {
        super.layout()
        videoLayer.position = .init(x: bounds.midX, y: bounds.midY)
        videoLayer!.bounds = bounds
        videoLayer.frame = frame
    }
}

/// View that displays video.
struct AVSampleBufferDisplay: NSViewRepresentable {
    typealias NSViewType = AVSampleBufferNSView

    var frame: AVSampleBufferDisplayLayer

    func makeNSView(context: Context) -> AVSampleBufferNSView {
        let view = AVSampleBufferNSView()
        view.wantsLayer = true
        view.videoLayer = frame
        view.layer?.insertSublayer(frame, at: 0)
        return view
    }

    func updateNSView(_ nsView: AVSampleBufferNSView, context: Context) {}
}
struct ContentView: View {
    let frameDisplay = AVSampleBufferDisplayLayer()
    var body: some View {
        AVSampleBufferDisplay(
            frame: frameDisplay
        ).onAppear { createData() }
    }
    func createData() {
        let data = UnsafeMutableRawPointer.allocate(
            byteCount: 90000, alignment: MemoryLayout<UInt8>.alignment)
        data.initializeMemory(as: UInt8.self, repeating: 20, count: 90000)

        var pxBuffer: CVPixelBuffer?
        CVPixelBufferCreateWithBytes(
            nil, 300, 300,
            // kCVPixelFormatType_8IndexedGray_WhiteIsZero works and is
            // displayed in AVSampleBufferDisplayLayer, but is inverted.
            //kCVPixelFormatType_OneComponent8 isn't displayed in AVSampleBufferDisplayLayer.
            kCVPixelFormatType_8IndexedGray_WhiteIsZero, data, 2,
            nil,
            nil, nil, &pxBuffer)

        let f = try! CMVideoFormatDescription(
            imageBuffer: pxBuffer!)

        var sampleTiming = CMSampleTimingInfo()
        sampleTiming.decodeTimeStamp = .invalid
        sampleTiming.presentationTimeStamp = CMClockGetTime(
            CMClockGetHostTimeClock())
        sampleTiming.duration = .invalid

        var bufferOut: CMSampleBuffer?
        _ = CMSampleBufferCreateReadyWithImageBuffer(
            allocator: kCFAllocatorDefault,
            imageBuffer: pxBuffer!, formatDescription: f,
            sampleTiming: &sampleTiming,
            sampleBufferOut: &bufferOut)

        frameDisplay.enqueue(bufferOut!)
    }
}

@main
struct cvPixelBufferPlaygroundApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
